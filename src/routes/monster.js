var express = require('express');
var router = express.Router();
var monster = require('../request/monster');
const axios = require('axios');
/**
 * This function comment is parsed by doctrine
 * @route GET /monsters
 * @group Monster
 * @param { string } name.query.require
 * @param { string } id.query.require
 * @param { string } climate.query.require
 * @param { string } environment.query.require
 * @param { string } type.query.require
 * @returns { Array }  200 - Array of Monster
 * @returns {Error}  default - Unexpected error
 */
router.get('',  function(req, res, next) {
    try {
        monster.find(req.query, res);
    } catch (error) {
        res.json(error);
    }
    /*try {
            const response = await axios.get('http://localhost:4000/monsters');
            console.log(response);
            res.send(response.data.json());
        } catch (error) {
            console.error(error);
        }*/
        //res.send(await axios.get('http://localhost:4000/monsters'))
});

module.exports = router;