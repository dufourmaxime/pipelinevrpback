var monsterSchema = ({
    source: {
        id: String,
    },
    name: String,
    cr: String,
    climate: String,
    environment: String,
    type: String,
});

module.exports = monsterSchema;