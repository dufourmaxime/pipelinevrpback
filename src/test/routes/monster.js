var chai = require ('chai');
var spies = require('chai-spies');
var chaiHttp = require ('chai-http');
var sinon = require('sinon');
require('dotenv').config();
var app = require ('../../app');
var rp = require('request-promise')
var { Monster } = require('../../model/monster');

// Configure chai
chai.use(spies);
chai.use(chaiHttp);
chai.should();

var admin = require("firebase-admin");
const uid = 'test-uid'

// describe("Monster route", () => {
//   describe("GET /monster", () => {
//     before(async () => {
//       try {
//         customToken = await admin.auth().createCustomToken(uid);
//         const res = await rp({
//           url: 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyCustomToken?key=AIzaSyDGtuzBKaNR2ouVRhdW1Nc7PW56jOoK32g',
//           method: 'POST',
//           body: {
//               token: customToken,
//               returnSecureToken: true,
//           },
//           json: true
//         });

//         idToken = res.idToken;
//       } catch (error) {
//       }
//     });

//     it("should call Monster.find", (done) => {    
//         var mock = sinon.stub(Monster, 'find').returns(true);
//         chai.request(app)
//             .get('/monster')
//             .set('authToken', idToken)
//             .end((err, res) => {
//                 sinon.assert.called(mock);
//                 done();
//             });
//     });

//     it("should return Unauthorized because no token", (done) => {
//       chai.request(app)
//         .get(`/monster`)
//         .end((err, res) => {
//           res.should.have.status(404);
//           done();
//         });
//     });
//   });
// });