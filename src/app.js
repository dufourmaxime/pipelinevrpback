var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var cors = require('cors');
var app = express();
require('dotenv').config()
const Sentry = require('@sentry/node');
const expressSwagger = require('express-swagger-generator')(app);

var monster = require('./routes/monster');

Sentry.init({ dsn: 'https://aed7ecf902bb47948c65d1d59e02df16@sentry.io/5175739', debug: true, environement: 'local' });

// Configuration SWAGGER
var configSwagger = require('./config/configSwagger');
expressSwagger(configSwagger);

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use(Sentry.Handlers.requestHandler());

app.use('/monsters', monster);

app.use(Sentry.Handlers.errorHandler());

app.use(function onError(err, req, res, next) {
  // The error id is attached to `res.sentry` to be returned
  // and optionally displayed to the user for support.
  res.statusCode = 500;
  res.end(res.sentry, "\n");
});

module.exports = app;
