var { Monster } = require('../model/monster');
var axios = require('axios');
var fetch = require('node-fetch');

exports.find = async (request, res) => {

    const monster = await getRequest();
    res.json(monster);
}

async function getRequest(){
    const result = await fetch('http://localhost:4000/monsters')
        .then(async (res) => {
            const response = await res.json();
            return response;
        })
        .catch((err) => ({
            message: 'Error getting request',
            error: err,
        }));
    return result;
}