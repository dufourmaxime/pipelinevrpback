import cors = require('cors');
require('dotenv').config();

// Start json-server
const jsonServer = require("json-server");
const server = jsonServer.create();
const router = jsonServer.router('db.json');
const middlewares = jsonServer.defaults();
const port = process.env.PORT;
const swagger = require('swagger-editor')(server);

// Configuration SWAGGER
var configSwagger = require('./config/configSwagger');
swagger('../swagger.json');

server.use(middlewares);
server.use(router);
server.use(cors());
server.listen(port, () => {
    console.log(`JSON Server is running on port ${port}`);
});

module.exports = server;