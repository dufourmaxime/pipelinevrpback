<img src="./src/public/asset/logo-voiceroleplay-light.svg" height="100" />

# Voice Role Pay

[![pipeline status](https://gitlab.com/voiceroleplay/vrp_back/badges/develop/pipeline.svg)](https://gitlab.com/voiceroleplay/vrp_back/-/commits/develop)
[![coverage report](https://gitlab.com/voiceroleplay/vrp_back/badges/develop/coverage.svg)](https://gitlab.com/voiceroleplay/vrp_back/-/commits/develop)


[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_back&metric=bugs)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_back)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_back&metric=code_smells)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_back)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_back&metric=coverage)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_back)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_back&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_back)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_back&metric=ncloc)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_back)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_back&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_back)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_back&metric=alert_status)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_back)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_back&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_back)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_back&metric=security_rating)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_back)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_back&metric=sqale_index)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_back)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=voiceroleplay_vrp_back&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=voiceroleplay_vrp_back)

Voice Role Play is the App Management for all your roleplay games.<br />
Add your own roleplay or directly join an existant on **Voice Role Play App**.

- Create your own roleplay
- Join an existant roleplay
- Customize your roleplay adventure
- Customize your roleplay characters
- Play your roleplay with **our voice assistant** 🎙


Back End app use:
- [x] Node JS 
- [x] Express
- [x] Chai.js
- [x] Mocha.js
- [x] Firebase Auth 🔐
- [x] Sentry 💬
- [ ] SonarCloud 🕵
- [ ] Gitlab CI 🚀


## Installation


Before installing, [download and install Node.js](https://nodejs.org/en/download/).


## Quick Start

  Install dependencies:

```bash
$ npm install
```

  Start the server:

```bash
$ npm run start
```

  Testing :
```bash
$ npm test
```

## SWAGGER


http://<app_host>:<app_port>/api-docs


## FRONT-END Auth

see here for informations :

https://itnext.io/how-to-use-firebase-auth-with-a-custom-node-backend-99a106376c8a



FireBase configuration :

```js
const firebaseConfig = {
  apiKey: "AIzaSyDGtuzBKaNR2ouVRhdW1Nc7PW56jOoK32g",
  authDomain: "voiceroleplay-b3ae9.firebaseapp.com",
  databaseURL: "https://voiceroleplay-b3ae9.firebaseio.com",
  projectId: "voiceroleplay-b3ae9",
  storageBucket: "voiceroleplay-b3ae9.appspot.com",
  messagingSenderId: "655583604240",
  appId: "1:655583604240:web:ab846e50b945f81a89e495",
  measurementId: "G-TS9BCSW7RL"
};
```


## License

  [MIT](LICENSE)